#!/usr/bin/env python3
#
# This file is part of OnionSpoutsBot, a Tor Browser distribution Telegram bot.
#
# :authors: See AUTHORS file for more information.
#
# :copyright:   (c) 2020-2023, Panagiotis "Ivory" Vasilopoulos, et al.
#
# :license: This is Free Software. See LICENSE for license information.
#

import asyncio
from typing import Optional

import aiohttp
import requests
from pyrogram import Client
from pyrogram.types import (CallbackQuery, InlineKeyboardButton,
                            InlineKeyboardMarkup)

from OnionSproutsBot import i18n

from .i18n import get_translation


# Helper function for updating the list of available binaries
async def get_response(endpoint):
    async with aiohttp.ClientSession() as session:
        async with session.get(endpoint) as resp:
            return await resp.json()


async def sig_send_notification(
    client: Client, callback: CallbackQuery, user_lang: str
):
    _ = get_translation(user_lang)

    return await client.send_message(
        callback.from_user.id, _("We are uploading the signature file first...")
    )


async def file_send_notification(
    client: Client, callback: CallbackQuery, user_lang: str
):
    _ = get_translation(user_lang)

    return await client.send_message(
        callback.from_user.id, _("We are now uploading the program...")
    )


async def upload_succeeded(
    client: Client,
    callback: CallbackQuery,
    user_lang: str,
    platform: str,
    locale: str,
):
    _ = get_translation(user_lang)

    await client.send_message(
        chat_id=callback.from_user.id,
        text=_(
            "<strong>Success!</strong>\n\n"
            "The version you have requested **(%s, %s)** has been "
            "successfully uploaded and sent to you. "
            "Stay safe!"
        )
        % (platform, locale),
        reply_markup=InlineKeyboardMarkup(
            [[InlineKeyboardButton(_("Main Menu"), "welcome:" + user_lang)]]
        ),
    )


async def upload_already_done(
    client: Client, callback: CallbackQuery, user_lang: str, platform: str, locale: str
):
    callback_data = callback.data.split(":")
    user_lang = callback_data[-1]
    _ = get_translation(user_lang)

    await client.send_message(
        chat_id=callback.from_user.id,
        text=_(
            "<strong>Success!</strong>\n\n"
            "We already had uploaded a copy of the version you "
            "requested **(%s, %s)**, so we sent it to you instantly. "
            "Stay safe!"
        )
        % (platform, locale),
        reply_markup=InlineKeyboardMarkup(
            [[InlineKeyboardButton(_("Main Menu"), "welcome:" + user_lang)]]
        ),
    )


async def upload_failed(
    client: Client,
    callback: CallbackQuery,
    lang: str,
    exception: Optional[Exception] = None,
    platform: Optional[str] = None,
    locale: Optional[str] = None,
):
    _ = get_translation(lang)

    """
    By default, we log a very limited amount of information that is normally enough to
    track down the root cause of an error. However, by providing a detailed,
    transparent error message and a point of contact, we allow the user to provide us
    with more context, establish whether an error may be occuring due to normal usage
    and more. This is likely the easiest way to do this in our situation.
    """

    exception = str(exception)
    exception_text = ""
    if Exception is not None and len(exception) > 0:
        exception_text = "\n\n" + _("Reason: ") + f"`{exception}`"

    download_info = ""
    if (platform is not None) and (locale is not None):
        download_info = f"(<strong>{platform}</strong>, <strong>{locale}</strong>)"

    await client.send_message(
        chat_id=callback.from_user.id,
        text=_(
            "<strong>Failure!</strong>\n\n"
            "Something went wrong during the upload! "
            "Please try again later. "
        )
        + exception_text
        + "\n\n☎️: @TorProjectSupportBot "
        + download_info,
        reply_markup=InlineKeyboardMarkup(
            [[InlineKeyboardButton(_("Main Menu"), "welcome:" + lang)]]
        ),
    )
