#!/usr/bin/env python3
#
# This file is part of OnionSpoutsBot, a Tor Browser distribution Telegram bot.
#
# :authors: See AUTHORS file for more information.
#
# :copyright:   (c) 2020-2023, Panagiotis "Ivory" Vasilopoulos, et al.
#
# :license: This is Free Software. See LICENSE for license information.
#

from typing import Optional

from pyrogram import Client
from pyrogram.types import (CallbackQuery, InlineKeyboardButton,
                            InlineKeyboardMarkup)

from OnionSproutsBot import i18n


# Example query: request_tor_mirrors:en
async def send_mirrors(client: Client, callback: CallbackQuery):
    callback_data = callback.data.split(":")
    lang = callback_data[-1]
    _ = i18n.get_translation(lang)

    await client.send_message(
        chat_id=callback.from_user.id,
        text="<strong>Tor Browser Mirrors</strong>\n\n"
        "- EFF: https://tor.eff.org\n"
        "- Calyx Institute: https://tor.calyxinstitute.org\n"
        "- GitHub: https://github.com/torproject/torbrowser-releases/releases/\n"
        "- Email: gettor@torproject.org "
        + _("(You should also include your operating system: Windows, macOS, or Linux)")
        + "\n\n"
        + _("(Make sure that your email provider is safe!)")
        + "\n",
        reply_markup=InlineKeyboardMarkup(
            [[InlineKeyboardButton(_("Main Menu"), "welcome:" + lang)]]
        ),
    )

    await callback.answer()


# Example query: request_tor_bridges:en
async def send_bridges(client: Client, callback: CallbackQuery):
    callback_data = callback.data.split(":")
    lang = callback_data[-1]
    _ = i18n.get_translation(lang)

    await client.send_message(
        chat_id=callback.from_user.id,
        text=_(
            "<strong>Tor Bridges</strong>\n\n"
            "Tor bridges are computers that can be very helpful when "
            "your internet provider has blocked you from using Tor, "
            "or if you just want to hide the fact that you are using Tor "
            "from anyone that could be monitoring your internet connection. "
            "That could be your internet provider, government, household "
            "members, school, workplace, among others.\n\n"
            "You can request a bridge from @getbridgesbot, but you should "
            "also configure the Tor Browser to use it."
        ),
        reply_markup=InlineKeyboardMarkup(
            [[InlineKeyboardButton(_("Main Menu"), "welcome:" + lang)]]
        ),
    )

    await callback.answer()


# Example query: explain_tor:en
async def explain_tor(client: Client, callback: CallbackQuery):
    callback_data = callback.data.split(":")
    lang = callback_data[-1]
    _ = i18n.get_translation(lang)

    await client.send_message(
        chat_id=callback.from_user.id,
        text=_(
            "<strong>What is Tor?</strong>\n\n"
            "Tor is a network that lets you anonymously connect to the Internet, "
            "while evading censorship, people monitoring of your Internet connection "
            "and ad tracking.\n\n"
            "The network is peer-to-peer and largely supported by volunteers, as "
            "well as The Tor Project Inc., a non-profit organization based "
            "in the United States."
        ),
        reply_markup=InlineKeyboardMarkup(
            [[InlineKeyboardButton(_("Main Menu"), "welcome:" + lang)]]
        ),
    )

    await callback.answer()
